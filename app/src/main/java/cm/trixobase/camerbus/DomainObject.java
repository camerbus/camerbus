package cm.trixobase.camerbus;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import cm.trixobase.camerbus.commom.ColumnName;
import cm.trixobase.camerbus.system.Camerbus;

/**
 * Created by noumianguebissie on 9/16/18.
 */

public abstract class DomainObject extends MySQLiteOpenHelper {

    protected long id = -1;
    protected ContentValues data;

    public abstract static class Builder<T extends DomainObject> {
        protected final T instance;

        protected Builder() {
            instance = newInstance();
            instance.data = new ContentValues();
        }

        protected abstract T newInstance();

        public Builder withData(ContentValues newData) {
            return this;
        }

        public T build() {
            return this.instance;
        }

    }

    protected DomainObject() {
        super(instanceContext);
    }

    public long getId() {
        return id;
    }

    protected ContentValues getData() {
        return data;
    }

    protected abstract String getTableName();

    protected long save() {
        try {
            if (getId() != -1) {
                instanceDatabase.update(getTableName(), data, ColumnName.ID + " = ?", new String[]{String.valueOf(getId())});
                Log.e(Camerbus.Log, "Update Succes in " + getTableName() + " id=" + getId());
            } else {
                id = instanceDatabase.insert(getTableName(), null, data);
                Log.e(Camerbus.Log, "Insert Succes in " + getTableName() + " :  id=" + getId());
            }
        } catch (SQLiteException ex) {
            Log.e(Camerbus.Log, "Exception SQL lors de la sauvegarde - " + ex);
        } catch (Exception e) {
            Log.e(Camerbus.Log, "Insert Error in "  + getTableName() + " : with id=" + getId() + "=> " + e);
        }
        return id;
    }

    protected long delete(long instanceId) {
        try {
            instanceDatabase.delete(getTableName(), ColumnName.ID + " = " + instanceId, null);
            Log.e(Camerbus.Log, "Delete Succes in " + getTableName() + " with id=" + instanceId);
            return 0;
        } catch (SQLiteException ex) {
            Log.e(Camerbus.Log, "Exception SQL lors de la suppression (" + getTableName() + "): " + ex);
        } catch (Exception e) {
            Log.e(Camerbus.Log, "Delete Error (" + getTableName() + ") - " + getTableName() + " " + e);
        }
        return -1;
    }

}
