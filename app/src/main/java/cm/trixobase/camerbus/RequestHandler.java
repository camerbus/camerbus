package cm.trixobase.camerbus;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import cm.trixobase.camerbus.commom.ColumnName;
import cm.trixobase.camerbus.commom.TableName;
import cm.trixobase.camerbus.system.User;
import cm.trixobase.camerbus.system.Camerbus;

/**
 * Created by noumianguebissie on 9/16/18.
 */

public abstract class RequestHandler extends DomainObject {

    private static Cursor cursor;

    public static boolean UserHasRegistered(Context context) {
        startTransaction(context);
        try {
            cursor = instanceDatabase.query(TableName.USER, null, null, null, null, null, null, "1");
            if (cursor.moveToFirst()) {
                Log.e(Camerbus.Log, "Utilisateur present !!!");
                return true;
            }
        } catch (Exception e) {
            Log.e(Camerbus.Log, "Echec lors de la verification de l'utilisateur: " + e);
        }
        Log.e(Camerbus.Log, "Pas d'utilisateur !!!");
        return false;
    }

    protected static User getAuthenticatedUser() {
        String selection = ColumnName.ID + " = ? ";
        String[] selectionArg = new String[]{"1"};

        try {
            cursor = instanceDatabase.query(TableName.USER, null, selection, selectionArg, null, null, null, "1");
            if (cursor.moveToFirst()) {
                return User.builder()
                        .identifiedById(cursor.getLong(cursor.getColumnIndexOrThrow(ColumnName.ID)))
                        .withName(cursor.getString(cursor.getColumnIndexOrThrow(ColumnName.USER_NAME)))
                        .withSurname(cursor.getString(cursor.getColumnIndexOrThrow(ColumnName.USER_SURNAME)))
                        .withNumber(cursor.getString(cursor.getColumnIndexOrThrow(ColumnName.USER_NUMBER)))
                        .withPassword(cursor.getString(cursor.getColumnIndexOrThrow(ColumnName.USER_PASSWORD)))
                        .withRegistrationDate(cursor.getString(cursor.getColumnIndexOrThrow(ColumnName.USER_REGISTRATION_DATE)))
                        .build();
            }
        } catch (Exception e) {
            Log.e(Camerbus.Log, "Echec lors de la recuperation des infos de l'utilisateur: " + e);
        } finally {
            Log.e(Camerbus.Log, "utilisateur trouve: " + cursor.getCount());
        }
        return null;
    }

}
