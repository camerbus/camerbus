package cm.trixobase.camerbus.system;

import android.content.Context;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by noumianguebissie on 9/16/18.
 */

public class Util {

    public static class date {

        public static String getDate(Calendar calendar) {
            return calendarToDate(calendar);
        }

        private static String calendarToDate(Calendar calendar) {
            String dayCount = formatNumber(String.valueOf(calendar.get(Calendar.DATE)));
            String month = formatNumber(String.valueOf(calendar.get(Calendar.MONTH) + 1));
            String year = formatNumber(String.valueOf(calendar.get(Calendar.YEAR)));
            return dayCount.concat("/").concat(month).concat("/").concat(year);
        }

    }

    public static void showToastLongMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static String formatNumber(String value) {
        return (value.length() == 1) ? 0 + value : value;
    }

}
