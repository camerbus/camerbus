package cm.trixobase.camerbus.system;

/**
 * Created by noumianguebissie on 9/16/18.
 */

public class Camerbus {

    public static final String Log = "CAMERBUS WARNING: ";

    public static final String Interface_To_Show = "Interface_To_Show";
    public static final String Interface_User = "Interface_User";
    public static final String Interface_Admin = "Interface_Admin";
}
