package cm.trixobase.camerbus.system;

import android.content.ContentValues;
import android.content.Context;


import cm.trixobase.camerbus.DomainObject;
import cm.trixobase.camerbus.commom.ColumnName;
import cm.trixobase.camerbus.commom.TableName;
import cm.trixobase.camerbus.RequestHandler;

/**
 * Created by noumianguebissie on 9/16/18.
 */

public class User extends RequestHandler {

    @Override
    protected String getTableName() {
        return TableName.USER;
    }

    public static class Builder extends DomainObject.Builder<User> {

        private Builder() {
            super();
        }

        @Override
        protected User newInstance() {
            return new User();
        }

        public Builder identifiedById(long id) {
            instance.data = new ContentValues();
            instance.id = id;
            return this;
        }

        public Builder withName(String name) {
            instance.name = name;
            return this;
        }

        public Builder withSurname(String surname) {
            instance.surname = surname;
            return this;
        }

        public Builder withNumber(String number) {
            instance.number = number;
            return this;
        }

        public Builder withPassword(String password) {
            instance.password = password;
            return this;
        }

        public Builder withRegistrationDate(String registrationDate) {
            instance.registrationDate = registrationDate;
            return this;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private User() {
        super();
    }

    public static User getOwner(Context context) {
        startTransaction(context);
        User userAuthenticated = RequestHandler.getAuthenticatedUser();
        stopTransaction();
        return userAuthenticated;
    }

    private String name;
    private String surname;
    public String number;
    private String password;
    public String registrationDate;

    public long save(Context context) {
        getData().put(ColumnName.USER_NAME, name);
        getData().put(ColumnName.USER_SURNAME, surname);
        getData().put(ColumnName.USER_NUMBER, number);
        getData().put(ColumnName.USER_PASSWORD, password);
        getData().put(ColumnName.USER_REGISTRATION_DATE, registrationDate);

        startTransaction(context);
        long userId = super.save();
        stopTransaction();

        return userId;
    }

}