package cm.trixobase.camerbus.system.datatable;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import cm.trixobase.camerbus.commom.ColumnName;
import cm.trixobase.camerbus.commom.TableName;
import cm.trixobase.camerbus.system.Camerbus;

/**
 * Created by noumianguebissie on 9/16/18.
 */

public class UserTable {

    private static final String TABLE_CREATE_USER = "create table " + TableName.USER + "("
            + ColumnName.ID + " integer primary key autoincrement, "
            + ColumnName.USER_NAME + " text not null, "
            + ColumnName.USER_SURNAME + " text not null, "
            + ColumnName.USER_NUMBER + " text not null, "
            + ColumnName.USER_PASSWORD + " text not null, "
            + ColumnName.USER_REGISTRATION_DATE + " text not null);";

    public static void createIn(SQLiteDatabase database) {
        try {
            database.execSQL(TABLE_CREATE_USER);
            Log.e(Camerbus.Log, "Create success: " + TableName.USER);
        } catch (Exception e) {
            Log.e(Camerbus.Log, "Create echec: " + TableName.USER);
        }

    }

}
