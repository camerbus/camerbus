package cm.trixobase.camerbus.commom;

/**
 * Created by noumianguebissie on 9/16/18.
 */

public class ColumnName {

    public static final String ID = "_id";

    public static final String USER_NAME = "c_name";
    public static final String USER_SURNAME = "c_surname";
    public static final String USER_NUMBER = "c_number";
    public static final String USER_PASSWORD = "c_password";
    public static final String USER_REGISTRATION_DATE = "c_date";

}
