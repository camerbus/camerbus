package cm.trixobase.camerbus.domain.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

import cm.trixobase.camerbus.R;
import cm.trixobase.camerbus.system.User;
import cm.trixobase.camerbus.system.Camerbus;
import cm.trixobase.camerbus.system.Util;
import cm.trixobase.camerbus.system.media.PhoneProcess;

public class Registration extends AppCompatActivity {

    private String name = "";
    private String surname = "";
    private String passwordToSave = "";

    private EditText et_name, et_surname, et_password, et_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.label_title_activity_registration));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        et_name = findViewById(R.id.et_registration_name_id);
        et_surname = findViewById(R.id.et_registration_surname_id);
        et_password = findViewById(R.id.et_registration_password_id);
        et_confirm = findViewById(R.id.et_registration_confirm_id);

        setupListener();
    }

    private void setupListener() {
        Button bt_signin = findViewById(R.id.bt_registration_signin);
        bt_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (canPass()) {
                    insertToFirebase();
                }
            }
        });
    }

    private boolean canPass() {
        name = et_name.getText().toString();
        surname = et_surname.getText().toString();
        String password = et_password.getText().toString();
        String confirm = et_confirm.getText().toString();

        EditText etName = findViewById(R.id.et_registration_name_id);
        EditText etSurname = findViewById(R.id.et_registration_surname_id);
        EditText etPassword = findViewById(R.id.et_registration_password_id);
        EditText etConfirm = findViewById(R.id.et_registration_confirm_id);

        if (name.isEmpty()) {
            showError(etName);
            showMessage(getResources().getString(R.string.error_user_save_name));
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
            return false;
        } else {
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
        }

        if (surname.isEmpty()) {
            showError(etSurname);
            showMessage(getResources().getString(R.string.error_user_save_surname));
            showAsValidate(etName);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
            return false;
        } else {
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
        }

        if (password.isEmpty()) {
            showError(etPassword);
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etConfirm);
            showMessage(getResources().getString(R.string.error_user_save_password));
            return false;
        } else {
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
        }

        if (password.length() < 6) {
            showMessage(getResources().getString(R.string.error_user_save_password_short));
            showError(etPassword);
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etConfirm);
            return false;
        } else {
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
        }

        if (password.length() > 10) {
            showMessage(getResources().getString(R.string.error_user_save_password_long));
            showError(etPassword);
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etConfirm);
            return false;
        } else {
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
        }

        if (confirm.isEmpty()) {
            showError(etConfirm);
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showMessage(getResources().getString(R.string.error_user_save_confirm_password));
            return false;
        } else {
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
        }

        if (!password.equalsIgnoreCase(confirm)) {
            showMessage(getResources().getString(R.string.error_user_save_confirm));
            showAsValidate(etName);
            showAsValidate(etSurname);
            showError(etPassword);
            showError(etConfirm);
            return false;
        } else {
            passwordToSave = password;
            showAsValidate(etName);
            showAsValidate(etSurname);
            showAsValidate(etPassword);
            showAsValidate(etConfirm);
        }
        return true;
    }

    private void insertToFirebase() {
        try {
    /*
       implement firebase
    */
            Log.e(Camerbus.Log, "Firebase success");
            User.builder()
                    .identifiedById(-1)
                    .withName(name)
                    .withSurname(surname)
                    .withNumber(PhoneProcess.getOwnerNumber(getBaseContext()))
                    .withPassword(passwordToSave)
                    .withRegistrationDate(Util.date.getDate(Calendar.getInstance()))
                    .build().save(getBaseContext());
        } catch (Exception e) {
            Log.e(Camerbus.Log, "Firebase Error - " + e);
        } finally {
            MainActivity.doFinish();
            Intent i = new Intent(getApplicationContext(), Home.class);
            startActivity(i);
            finish();
        }
    }

    private void showError(EditText editText) {
        editText.setTextColor(getResources().getColor(R.color.widget_form_error));
        editText.setHintTextColor(getResources().getColor(R.color.widget_form_error));
    }

    private void showAsValidate(EditText editText) {
        editText.setHintTextColor(getResources().getColor(R.color.widget_text_color));
        editText.setTextColor(getResources().getColor(R.color.widget_text_color));
    }

    private void showMessage(String message) {
        Util.showToastLongMessage(getBaseContext(), message);
    }
}
