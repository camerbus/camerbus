package cm.trixobase.camerbus.domain.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import cm.trixobase.camerbus.R;
import cm.trixobase.camerbus.RequestHandler;

public class MainActivity extends AppCompatActivity {

    private static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        if (RequestHandler.UserHasRegistered(getBaseContext()))
            goToHome();

        setupListener();
    }

    private void setupListener() {
        Button bt_createAccount = findViewById(R.id.bt_main_create_account_id);
        bt_createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),Registration.class);
                startActivity(intent);
            }
        });

        Button bt_connect = findViewById(R.id.bt_main_connect_id);
        bt_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),Connexion.class);
                startActivity(intent);
            }
        });
    }

    private void goToHome() {
        Intent i = new Intent(getBaseContext(), Home.class);
        startActivity(i);
        finish();
    }

    public static void doFinish() {
        activity.finish();
    }
}
