package cm.trixobase.camerbus.domain.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cm.trixobase.camerbus.R;
import cm.trixobase.camerbus.system.Camerbus;
import cm.trixobase.camerbus.system.User;
import cm.trixobase.camerbus.system.Util;

public class Connexion extends AppCompatActivity {

    private String name = "";
    private String surname = "";
    private String number = "";
    private String password = "";
    private String registrationDate = "";

    private EditText et_number, et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        et_number = findViewById(R.id.et_connexion_number_id);
        et_password = findViewById(R.id.et_connexion_password_id);

        setupListener();

    }

    private void setupListener() {
        Button bt_connect = findViewById(R.id.bt_connexion_connect);
        bt_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!canStartConnect())
                    return;
                if (userHasIdentified()) {
                    User.builder()
                            .identifiedById(-1)
                            .withName(name)
                            .withNumber(number)
                            .withPassword(password)
                            .withSurname(surname)
                            .withRegistrationDate(registrationDate)
                            .build().save(getBaseContext());
                    Intent intent = new Intent(getBaseContext(), Home.class);
                    startActivity(intent);
                    MainActivity.doFinish();
                    finish();
                }
                Intent intent = new Intent(getBaseContext(), Home.class);
                startActivity(intent);
                MainActivity.doFinish();
                finish();
            }
        });
    }

    private boolean userHasIdentified() {
        try {
            /*
            implement connexion to firebase and different offside option

            Log.e(Camerbus.Log, "Connexion to firebase success");
            return true;
             */
        } catch (Exception e) {
            Log.e(Camerbus.Log, "echec to authenticate user in firebase" + e);
        }
        return false;
    }

    private boolean canStartConnect() {
        number = et_number.getText().toString();
        password = et_password.getText().toString();

        EditText etnumber = findViewById(R.id.et_connexion_number_id);
        EditText etpassword = findViewById(R.id.et_connexion_password_id);

        if (number.isEmpty()) {
            showError(etnumber);
            showMessage(getResources().getString(R.string.error_user_save_number));
            showAsValidate(etpassword);
            return false;
        } else {
            showAsValidate(etnumber);
            showAsValidate(etpassword);
        }

        if (number.length() < 9) {
            showMessage(getResources().getString(R.string.error_user_save_number_short));
            showError(etnumber);
            showAsValidate(etpassword);
            return false;
        } else {
            showAsValidate(etnumber);
            showAsValidate(etpassword);
        }

        if (number.length() > 9) {
            showMessage(getResources().getString(R.string.error_user_save_number_long));
            showError(etpassword);
            showAsValidate(etnumber);
            return false;
        } else {
            showAsValidate(etnumber);
            showAsValidate(etpassword);
        }

        if (password.isEmpty()) {
            showError(etpassword);
            showMessage(getResources().getString(R.string.error_user_save_password));
            showAsValidate(etnumber);
            return false;
        } else {
            showAsValidate(etnumber);
            showAsValidate(etpassword);
        }

        if (password.length() < 6) {
            showMessage(getResources().getString(R.string.error_user_save_password_short));
            showError(etpassword);
            showAsValidate(etnumber);
            return false;
        } else {
            showAsValidate(etpassword);
            showAsValidate(etnumber);
        }

        if (password.length() > 10) {
            showMessage(getResources().getString(R.string.error_user_save_password_long));
            showError(etpassword);
            showAsValidate(etnumber);
            return false;
        } else {
            showAsValidate(etpassword);
            showAsValidate(etnumber);
        }

        return true;
    }

    private void showError(EditText editText) {
        editText.setTextColor(getResources().getColor(R.color.widget_form_error));
        editText.setHintTextColor(getResources().getColor(R.color.widget_form_error));
    }

    private void showAsValidate(EditText editText) {
        editText.setHintTextColor(getResources().getColor(R.color.widget_text_color));
        editText.setTextColor(getResources().getColor(R.color.widget_text_color));
    }

    private void showMessage(String message) {
        Util.showToastLongMessage(getBaseContext(), message);
    }
}
